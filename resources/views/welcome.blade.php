<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bukku</title>
  <link rel="shortcut icon" href="public/images/favicon.jpg" type="image/jpg">

  <!-- Jquery UI  -->
  <link rel="stylesheet" href="public/css/jquery-ui.min.css">

  <!-- Bootstrap  -->
  <link rel="stylesheet" href="public/css/bootstrap.min.css">

  <!-- Material Icons  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Style -->
  <link rel="stylesheet" href="public/css/main.css">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

  <header>
    <nav>
      <div class="head-nav">
        <div class="container-fluid">

       

                

       
          <ul class="list-unstyled list-inline">
            <li>
              <a href="#">
                <strong>Customer Care</strong>
              </a>
            </li>
            @if (Route::has('login'))
            @auth
            <li>
              <a href="/home">
                <strong>Akunku</strong>
              </a>
            </li>
            @else
            <li>
              <a href="#daftar" data-toggle="modal" data-dismiss="modal">
                <strong>Daftar</strong>
              </a>
            </li>
            <li>
              <a href="#masuk" data-toggle="modal" data-dismiss="modal">
                <strong>Masuk</strong>
              </a>
            </li>
            @endauth
            @endif
          </ul>
        </div>
      </div>
      <!-- .head-nav -->

      <div class="navbar">
        <div class="container-fluid">
          <div class="col-lg-2 visible-lg">
            <a class="navbar-brand" href="#">
              <img class="img-responsive center-block" src="public/images/logo.png" alt="bukku.co.id">
            </a>
          </div>

          <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 text-center">
            <a href="">Kategori</a>
          </div>

          <div class="col-xxs-12 col-xs-7 col-sm-6 col-md-7 col-lg-6">
            <div class="input-group has-feedback">
              <input type="search" class="form-control" id="search-in-nav">
              <img class="form-control-feedback hidden-lg" src="public/images/logo.png" alt="bukku.co.id" id="logo-in-search">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                  <i class="material-icons">search</i>
                </button>
              </span>
            </div>
          </div>

          <div class="col-xxs-12 col-xs-5 col-sm-4 col-md-3 col-lg-2">
            <ul class="list-styled list-inline">
              <li>
                <a href="">
                  <i class="material-icons">face</i>
                </a>
              </li>
              <li>
                <a href="">
                  <i class="material-icons">favorite</i>
                </a>
              </li>
              <li>
                <a href="">
                  <i class="material-icons">shopping_cart</i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!-- .continer-fluid -->
      </div>
      <!-- .navbar -->

    

      <div class="modal fade verify-box" id="masuk">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close visible-xs" data-dismiss="modal" aria-hidden="true">&times;</button>
              <div class="login-logo">
                <img class="img-responsive center-block" src="public/images/logo.png" alt="bukku.co.id">
              </div>

              <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group">
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" id="login-email">
                </div>
                <!-- .form-group -->

                <div class="form-group">
                  <input type="password" name="password"  class="form-control show-password" placeholder="Kata Sandi" id="login-password">
                  <img class="img-responsive center-block show-hide-button" src="public/images/show.png" alt="showandhide">
                </div>
                <!-- .form-group -->

                <div class="col-xs-6">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" value="">
                      <span class="cr">
                        <i class="cr-icon fa fa-check"></i>
                      </span>
                      Ingat Saya
                    </label>
                  </div>
                </div>
                <!-- .columns -->

                <div class="col-xs-6 text-right">
                  <a href="#">Lupa kata sandi?</a>
                </div>
                <!-- .columns -->

                <button type="submit" class="btn btn-primary btn-login">Masuk ke bukku.co.id</button>
              </form>

              <div class="line">
                <span>atau</span>
              </div>

              <button type="button" class="btn btn-default">
                <img src="public/images/google.png" alt="google"> Masuk dengan Google</button>
              <button type="button" class="btn btn-default">
                <img src="public/images/facebook.png" alt="facebook"> Masuk dengan Facebook</button>
              <button type="button" class="btn btn-default">
                <img src="public/images/mobile.png" alt="mobile"> Masuk dengan Nomor Ponsel</button>

              <div class="text-center">
                Belum punya akun bukku? daftar
                <a href="#daftar" data-toggle="modal" data-dismiss="modal">disini</a>
              </div>
            </div>
            <!-- .modal-body -->
          </div>
        </div>
      </div>
      <!-- .modal -->

      <div class="modal fade verify-box" id="daftar">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close visible-xs" data-dismiss="modal" aria-hidden="true">&times;</button>
              <div class="login-logo">
                <img class="img-responsive center-block" src="public/images/logo.png" alt="bukku.co.id">
              </div>

              <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Nama Lengkap" id="register-name" name="name" value="{{ old('name') }}">
                  <div class="register-name-alert">Nama lengkap harus diisi</div>
                </div>
                <!-- .form-group -->

                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Nomor Telepon" id="register-number">
                  <p class="note-form">Pastikan nomor telepon Anda aktif untuk keamanan dan kemudahan transaksi</p>
                  <div class="register-number-alert">Nomor telepon harus diisi</div>
                </div>
                <!-- .form-group -->

                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Email" id="register-email" name="email" value="{{ old('email') }}">
                  <div class="register-email-alert">Email harus diisi</div>
                </div>
                <!-- .form-group -->

                <div class="form-group">
                  <input type="password" class="form-control show-password" placeholder="Kata Sandi" id="register-password" name="password">
                  <img class="img-responsive center-block show-hide-button" src="public/images/show.png" alt="showandhide">
                  <p class="note-form">Minimal 6 karakter</p>
                  <div class="register-password-alert">Kata sandi harus diisi</div>
                </div>
                <!-- .form-group -->

                <button type="submit" class="btn btn-primary btn-login">Daftar ke bukku.co.id</button>
              </form>

              <div class="agreement">
                <p>Dengan menekan Daftar akun, saya mengkonfirmasi bahwa Anda telah menyetujui
                  <a href="#">
                    <strong>Syarat dan Ketentuan</strong>
                  </a> serta
                  <a href="#">
                    <strong>Kebijakan Privasi</strong>
                  </a> Bukku.co.id</p>
              </div>

              <div class="line">
                <span>atau</span>
              </div>

              <button type="button" class="btn btn-default">
                <img src="public/images/google.png" alt="google"> Masuk dengan Google</button>
              <button type="button" class="btn btn-default">
                <img src="public/images/facebook.png" alt="facebook"> Masuk dengan Facebook</button>

              <div class="text-center">
                Sudah punya akun bukku? masuk
                <a href="#masuk" data-toggle="modal" data-dismiss="modal">disini</a>
              </div>
            </div>
            <!-- .modal-body -->
          </div>
        </div>
      </div>
      <!-- .modal -->

    </nav>
  </header>

  <main>
    <section>
      <div class="container">
        <h1 class="text-center">Bismillah</h1>
      </div>
      <!-- .container -->
    </section>

    <section class="one" id="one">
      <div class="container">
        <h2>Title</h2>
        <p>Ullamco excepteur deserunt Lorem ullamco minim laboris quis. Culpa consectetur officia aliqua dolor sit et labore
          ipsum. Elit in irure do reprehenderit consequat in deserunt ex aute ut ex sint minim. Minim commodo enim tempor
          amet ullamco. Duis magna culpa voluptate eiusmod in labore nostrud Lorem consequat cupidatat deserunt excepteur.</p>
      </div>
      <!-- .container -->
    </section>

    <section class="two" id="two">
      <div class="container">
        <h2>Title</h2>
        <p>Ullamco excepteur deserunt Lorem ullamco minim laboris quis. Culpa consectetur officia aliqua dolor sit et labore
          ipsum. Elit in irure do reprehenderit consequat in deserunt ex aute ut ex sint minim. Minim commodo enim tempor
          amet ullamco. Duis magna culpa voluptate eiusmod in labore nostrud Lorem consequat cupidatat deserunt excepteur.</p>
      </div>
      <!-- .container -->
    </section>
  </main>

  <footer>
    <div class="head-footer">
      <div class="container-fluid">
        <div class="col-sm-3 about-in-footer">
          <h4>Tentang</h4>
          <img class="img-responsive" src="public/images/footer-logo.png" alt="bukku.co.id">
          <p class="excerpt-about-in-footer">
            Bukku.co.id adalah situs #BelanjaBukku online, dengan jangkauan distribusi luas ke seluruh Indonesia.
          </p>
        </div>
        <!-- .columns -->

        <div class="col-sm-3 help-in-footer">
          <h4>Bantuan</h4>
          <ul class="list-unstyled">
            <li>
              <a href="#">Syarat dan Ketentuan</a>
            </li>
            <li>
              <a href="#">Kebijakan Privasi</a>
            </li>
            <li>
              <a href="#">Pusat Resolusi</a>
            </li>
            <li>
              <a href="#">Hubungi Kami</a>
            </li>
            <li>
              <a href="#">Panduan Keamanan</a>
            </li>
          </ul>
        </div>
        <!-- .columns -->

        <div class="col-sm-3 contact-in-footer">
          <h4>Kontak</h4>
          <ul class="list-unstyled">
            <li>
              <a href="">
                <img src="public/images/whatsapp.png" alt="whatsapp">
              </a>
            </li>

            <li>
              <a href="">
                <img src="public/images/line.png" alt="line">
              </a>
            </li>
          </ul>
        </div>
        <!-- .columns -->

        <div class="col-sm-3 find-in-footer">
          <h4>Temukan Kami</h4>
          <ul class="list-unstyled">
            <li>
              <a href="">
                <img src="public/images/facebook.png" alt="facebook"> Facebook
              </a>
            </li>

            <li>
              <a href="">
                <img src="public/images/instagram.png" alt="instagram"> Instagram
              </a>
            </li>

            <li>
              <a href="">
                <img src="public/images/youtube.png" alt="Youtube"> Youtube
              </a>
            </li>
          </ul>
        </div>
        <!-- .columns -->
      </div>
      <!-- .container-fluid -->
    </div>
    <!-- .head-footer -->

    <div class="col-xs-12 copyright">
      <div class="container-fluid">
        <p>Bukku &copy; 2017 Copyright Holder All Rights Reserved. </p>
      </div>
      <!-- .container-fluid -->
    </div>
  </footer>

  <!-- Jquery  -->
  <script src="public/js/jquery-3.2.1.min.js"></script>

  <!-- Jquery UI -->
  <script src="public/js/jquery-ui.min.js"></script>

  <!-- Bootstrap  -->
  <script src="public/js/bootstrap.min.js"></script>

  <!-- Main  -->
  <script src="public/js/main.js"></script>


  
  
  
  

</body>

</html>
