"use strict";

var loginShowAndHideButton = document.querySelector("#masuk .show-hide-button");
var loginFormPassword = document.querySelector("#masuk .show-password");

var registerShowAndHideButton = document.querySelector("#daftar .show-hide-button");
var registerFormPassword = document.querySelector("#daftar .show-password");
var registerName = document.getElementById("register-name");
var registerNumber = document.getElementById("register-number");
var registerEmail = document.getElementById("register-email");
var registerPassword = document.getElementById("register-password");

var searchInNav = document.getElementById("search-in-nav");
var logoInSearch = document.getElementById("logo-in-search");

var lengthWidth = window.innerWidth;

loginShowAndHideButton.addEventListener("click", function () {
    if (loginFormPassword.getAttribute("class") === "form-control show-password") {
        loginFormPassword.setAttribute("class", "form-control hide-password");
        loginFormPassword.setAttribute("type", "text");
        loginShowAndHideButton.src = "public/images/hide.png";
        loginShowAndHideButton.style.top = "9.5px";
    } else {
        loginFormPassword.setAttribute("class", "form-control show-password");
        loginFormPassword.setAttribute("type", "password");
        loginShowAndHideButton.src = "public/images/show.png";
        loginShowAndHideButton.style.top = "13px";
    }
});

registerName.addEventListener("change", function () {
    var registerNameAlert = document.querySelector(".register-name-alert");
    if (registerName.value.length < 6) {
        registerName.style.borderColor = "#e74c3c";
        registerNameAlert.style.display = "block";
    } else {
        registerName.style.borderColor = "#ccc";
        registerNameAlert.style.display = "none";
    }
});

registerNumber.addEventListener("change", function () {
    var registerNumberAlert = document.querySelector(".register-number-alert");
    if (registerNumber.value.length < 6) {
        registerNumber.style.borderColor = "#e74c3c";
        registerNumberAlert.style.display = "block";
    } else {
        registerNumber.style.borderColor = "#ccc";
        registerNumberAlert.style.display = "none";
    }
});

registerEmail.addEventListener("change", function () {
    var registerEmailAlert = document.querySelector(".register-email-alert");
    if (registerEmail.value.length < 6) {
        registerEmail.style.borderColor = "#e74c3c";
        registerEmailAlert.style.display = "block";
    } else {
        registerEmail.style.borderColor = "#ccc";
        registerEmailAlert.style.display = "none";
    }
});

registerPassword.addEventListener("change", function () {
    var registerPasswordAlert = document.querySelector(".register-password-alert");
    if (registerPassword.value.length < 6) {
        registerPassword.style.borderColor = "#e74c3c";
        registerPasswordAlert.style.display = "block";
    } else {
        registerPassword.style.borderColor = "#ccc";
        registerPasswordAlert.style.display = "none";
    }
});

registerShowAndHideButton.addEventListener("click", function () {
    if (registerFormPassword.getAttribute("class") === "form-control show-password") {
        registerFormPassword.setAttribute("class", "form-control hide-password");
        registerFormPassword.setAttribute("type", "text");
        registerShowAndHideButton.src = "public/images/hide.png";
        registerShowAndHideButton.style.top = "9.5px";
    } else {
        registerFormPassword.setAttribute("class", "form-control show-password");
        registerFormPassword.setAttribute("type", "password");
        registerShowAndHideButton.src = "public/images/show.png";
        registerShowAndHideButton.style.top = "13px";
    }
});

if (lengthWidth >= 1200) {
    searchInNav.setAttribute("placeholder", "Cari produk, kategori, penerbit, atau, penulis");
} else if (lengthWidth < 1200) {
    searchInNav.addEventListener("click", function () {
        logoInSearch.style.display = "none";
    });

    searchInNav.addEventListener("change", function () {
        if (searchInNav.value.length < 1) {
            logoInSearch.style.display = "inline-block";
        }
    });
}